$(document).ready ->
  Parse.initialize("A7mEvSjXsC4DBSaSwj6hOSUtixtqe5cY3H9Rooxx",
    "tYrSOcW4dXWjIN1yHxvY8oE9W18sUBJPCUtd0NPj")

  Deal = Parse.Object.extend("Deal")

  $('.datetimepicker').datetimepicker({
    language: 'en',
    pick12HourFormat: true
  })

  $('#deal-submit').click ((e) ->
    e.preventDefault()
    deal = new Deal()

    # Iterate through key-value pairs of form
    formValues = $('#deal-form').serializeArray()
    for a in formValues
      # TODO: Add support for pictures
      deal.set(a.name, a.value) if a.value
    deal.save()

    # TODO: Redirect elsewhere
    # window.location.href = todo
  )
